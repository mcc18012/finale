<%--
  Created by IntelliJ IDEA.
  User: McCleve Family
  Date: 12/8/2020
  Time: 9:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>HomePage.jsp</title>
    <style><%@include file="/WEB-INF/old.scss"%></style>
</head>
<body>
<h3>SCiPNET TERMINAL #019-004</h3>
<div>
    <p>----------</p><p>Welcome, Dr. McCleve, M.D., Ph.D., Psy.D., D.F.A, D.H.L.</p><p>----------</p>
    <br>
    <p>Notice: (1) item needs your review, Dr. McCleve. Please access your inbox when available.</p>
</div>

<div>
    <p>This terminal has limited information due to the nature of certain SCP objects stored at Site-19.</p>
    <p>Please select one of the following options:</p>
    <p>Access: <a href="${pageContext.request.contextPath}/ListPageForm.jsp">available SCP files from database</a></p>
    <p>Access: Security controls on containment chambers (temporarily disabled)</p>
    <p><b>Inbox (1)</b><a href="${pageContext.request.contextPath}/MadLib.jsp">Subject: Overdue paperwork</a></p>
</div>

</body>
</html>
