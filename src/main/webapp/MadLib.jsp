<%--
  Created by IntelliJ IDEA.
  User: McCleve Family
  Date: 12/15/2020
  Time: 10:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mad Lib Stuff</title>
    <style><%@include file="/WEB-INF/old.scss"%></style>
</head>
<body>

<h2>WARNING: SCP-XXX-J is highly confidential. Please fill out Form- &#9608;&#9608;&#9608;&#9608;-28-J
    before proceeding.</h2>
<br>
<br>

<form action="Folder" method="post">
    <table>
        <thead><b>Threat:</b></thead>
        <tbody>
        <tr><td>
            <input type="radio" id="Safe" name="threat" value="Safe" required><label for="Safe">
            Safe (Easily and safely contained)</label>
        </td></tr>
        <tr><td>
            <input type="radio" id="Euclid" name="threat" value="Euclid"><label for="Euclid">
            Euclid (Containment isn't always reliable)</label>
            <td></tr>
        <tr><td>
            <input type="radio" id="Keter" name="threat" value="Keter"><label for="Keter">
            Keter (Exceedingly difficult to contain consistently/reliably)</label>
        </td></tr>
        </tbody>
    </table>
    <br>
    <table>
        <thead><b>Properties:</b></thead>
        <tbody><tr><input type="checkbox" name="tags" value="alive"> alive</tr>
        <tr><input type="checkbox" name="tags" value="extradimensional"> extradimensional</tr>
        <tr><input type="checkbox" name="tags" value="hostile"> hostile</tr>
        <tr><input type="checkbox" name="tags" value="nocturnal"> nocturnal</tr>
        <tr><input type="checkbox" name="tags" value="sentient"> sentient</tr>
        <tr><input type="checkbox" name="tags" value="simian"> simian</tr>
        <tr><input type="checkbox" name="tags" value="unpredictable"> unpredictable</tr>
        <tr><input type="checkbox" name="tags" value="xenarthran"> xenarthran</tr>
        </tbody>
    </table>
    <table>
        <tr>
            <td>Your Last Name:</td>
            <td><input type="text" name="lastName"></td>
            <td>Clearance Level:</td>
            <td>
                <select name="level">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>An Odd Number:</td>
            <td><input type="text" name="oddNum"></td>
            <td>A Profession (Plural):</td>
            <td><input type="text" name="profession"></td>
        </tr>
        <tr>
            <td>A noun:</td>
            <td><input type="text" name="noun"></td>
            <td>A Type of Food:</td>
            <td><input type="text" name="foodType"></td>
        </tr>
        <tr>
            <td>Body Part:</td>
            <td><input type="text" name="bodyPart"></td>
            <td>A Large Number:</td>
            <td><input type="text" name="largeNum"></td>
        </tr>
        <tr>
            <td>Small Number Greater Than 1:</td>
            <td><input type="text" name="smallNum"></td>
            <td>Something Cheap (plural):</td>
            <td><input type="text" name="cheap"></td>
        </tr>
        <tr>
            <td>An "-ing" Verb:</td>
            <td><input type="text" name="ingVerb1"></td>
            <td>Verb:</td>
            <td><input type="text" name="verb1"></td>
        </tr>
        <tr>
            <td>A Greek Letter (ie alpha):</td>
            <td><input type="text" name="greek"></td>
            <td>Your Favorite TV Show:</td>
            <td><input type="text" name="tv"></td>
        </tr>
        <tr>
            <td>Adjective:</td>
            <td><input type="text" name="adj1"></td>
            <td>Your Hometown:</td>
            <td><input type="text" name="hometown"></td>
        </tr>
        <tr>
            <td>A Noun:</td>
            <td><input type="text" name="noun1"></td>
            <td>Your Best Friend's last name:</td>
            <td><input type="text" name="bestFriend"></td>
        </tr>
        <tr>
            <td>A Football Team (eg Packers):</td>
            <td><input type="text" name="football"></td>
            <td>An Adjective:</td>
            <td><input type="text" name="adj1"></td>
        </tr>
        <tr>
            <td>A Verb and a Noun:</td>
            <td><input type="text" name="verbNoun"></td>
            <td>Another Body Part:</td>
            <td><input type="text" name="bodyPart2"></td>
        </tr>
        <tr>
            <td>Somewhere heavily guarded:</td>
            <td><input type="text" name="guarded"></td>
            <td>An Animal:</td>
            <td><input type="text" name="animal"></td>
        </tr>
        <tr>
            <td>A Famous Person:</td>
            <td><input type="text" name="famous"></td>
            <td>An Even Number:</td>
            <td><input type="text" name="evenNum"></td>
        </tr>
        <tr>
            <td>Another Body Part:</td>
            <td><input type="text" name="bodyPart2"></td>
            <td>Another Adjective:</td>
            <td><input type="text" name="adj2"></td>
        </tr>
        <tr>
            <td>A German Last Name:</td>
            <td><input type="text" name="german"></td>
            <td>Another "-ing" Verb:</td>
            <td><input type="text" name="ingVerb2"></td>
        </tr>
        <tr>
            <td>Non-Proper Plural Noun:</td>
            <td><input type="text" name="nonprop1"></td>
            <td>Another Noun:</td>
            <td><input type="text" name="noun2"></td>
        </tr>
        <tr>
            <td>A Cartoon-ish Villain Goal (take over the world):</td>
            <td><input type="text" name="takeOver"></td>
            <td>Yet Another Noun:</td>
            <td><input type="text" name="noun3"></td>
        </tr>
        <tr>
            <td>Another Verb:</td>
            <td><input type="text" name="verb2"></td>
            <td>Yet Another Verb:</td>
            <td><input type="text" name="verb3"></td>
        </tr>
        <tr>
            <td>Another Non-Proper Plural Noun:</td>
            <td><input type="text" name="nonprop2"></td>
            <td>Nothing. Just want it to be even:</td>
            <td><input type="submit" value="Submit"></td>
        </tr>
    </table>
</form>

</body>
</html>