package WhatInThe;

import org.json.JSONArray;
import org.json.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@WebServlet(name="WhatInThe.ListPage", urlPatterns ={"/WhatInThe"})
public class ListPage extends HttpServlet {
    private int index;
    private String getEntry;
    Assign choice = new Assign();

    public ListPage() {
        System.out.println("ListPage constructor called");
    }

    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("Calling ListPage's \"init\" method");
    }

    public void destroy() {
        System.out.println("ListPage's \"Destroy\" method called");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("ListPage \"DoGet\" method called");
        System.out.println("calling readIt(), which does stuff");

        try {
            String dank = request.getParameter("butt");
            System.out.println(dank);
        } catch (NullPointerException e) {
            System.out.println("That didn't work. currently dank is null");
        }

        switch (request.getParameter("butt")) {
            case "006":
                getEntry = "006";
                break;
            case "173":
                getEntry = "173";
                break;
            case "294":
                getEntry = "294";
                break;
            case "682":
                getEntry = "682";
                break;
            default:
                getEntry = "lazy";
                break;
        }
        System.out.println(getEntry);
        System.out.println("calling readIt, which identifies the choice and pulls the relevant info");

        try {
            readIt(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void time() throws InterruptedException {
        Thread.sleep(2000);
    }

    public void readIt(HttpServletRequest request, HttpServletResponse response) throws Exception {

        PrintWriter out = response.getWriter();
        switch (getEntry) {
            case "006":
                index = 0;
                break;
            case "173":
                index = 1;
                break;
            case "294":
                index = 2;
                break;
            case "682":
                index = 3;
                break;
            default:
                index = 4;
                break;
        }
        System.out.println(index);
        Thread.sleep(3000);

        String file = request.getServletContext().getRealPath("/directory.json");
        String json = readItQuick(file);
        System.out.println(json);

        JSONArray array = new JSONArray(json);
            JSONObject superjob = array.getJSONObject(index);
            choice.setId(superjob.getString("id"));
            choice.setThreat((String) superjob.get("threat"));
            choice.setProcedures(superjob.getJSONArray("procedures"));
        JSONArray jPro = (JSONArray) superjob.get("procedures");
        Iterator<Object> piterator = jPro.iterator();
            choice.setDescription(superjob.getJSONArray("description"));
        JSONArray jDes = (JSONArray) superjob.get("description");
        Iterator<Object> diterator = jDes.iterator();
            choice.setAddendum(superjob.getJSONArray("addendum"));
            choice.setStatus((boolean) superjob.get("status"));
            JSONArray jAdd = (JSONArray) superjob.get("addendum");
            Iterator<Object> iterator = jAdd.iterator();
        out.println("<html><head></head><body>");
        out.println("<p><b>Item #:</b> " + choice.getId() + "</p><p><b>Object Class:</b> " + choice.getThreat() +
                "</p><p><b>Special Containment Procedures:</b></p>");
while (piterator.hasNext()) {
    out.println(piterator.next() + "<br>");
        }
        out.println("<p><b>Description:</b></p>");
while (diterator.hasNext()) {
    out.println(diterator.next() + "<br>");
}
out.println("</p><p><b>Containment Status:</b> " + choice.getStatus() + "</p>");
        while (iterator.hasNext()) {
            out.println(iterator.next() + "<br>");
        }
        out.println("</body></html>");
    }

    public static String readItQuick(String file)throws Exception
    {
        return new String(Files.readAllBytes(Paths.get(file)));
    }
}