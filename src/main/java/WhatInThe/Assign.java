package WhatInThe;

import org.json.JSONArray;

public class Assign {
    
        private String id;
        private String threat;
        private JSONArray procedures;
        private JSONArray description;
        private JSONArray addendum;
        private boolean status;

        public String getId() {
            return this.id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getThreat() {
            return this.threat;
        }

        public void setThreat(String threat) {
            this.threat = threat;
        }

        public JSONArray getProcedures() {
            return this.procedures;
        }

        public void setProcedures(JSONArray procedures) {
            this.procedures = procedures;
        }

        public JSONArray getDescription() {
            return this.description;
        }

        public void setDescription(JSONArray description) {
            this.description = description;
        }

        public JSONArray getAddendum() {
                return this.addendum;
        }

        public void setAddendum(JSONArray addendum) {
            this.addendum = addendum;
        }

        public boolean getStatus() {
            return this.status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public void getItDone(String id, String threat, JSONArray procedures, JSONArray description,
                              JSONArray addendum, boolean status) {
            this.id = id;
            this.threat = threat;
            this.procedures = procedures;
            this.description = description;
            this.addendum = addendum;
            this.status = status;
        }

        public Assign() {

        }
}
