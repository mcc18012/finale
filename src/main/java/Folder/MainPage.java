package Folder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="Folder.MainPage", urlPatterns="/Folder")
public class MainPage extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,
            NullPointerException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String lastName = request.getParameter("lastName");
        String profession = request.getParameter("profession");
        String foodType = request.getParameter("foodType");
        String bodyPart = request.getParameter("bodyPart");
        String threat = request.getParameter("threat");
        String[] tags = request.getParameterValues("tags");
        String hometown = request.getParameter("hometown");
        String cheap = request.getParameter("cheap");
        String ingVerb1 = request.getParameter("ingVerb1");
        String verb1 = request.getParameter("verb1");
        String greek = request.getParameter("greek");
        String tv = request.getParameter("tv");
        String level = request.getParameter("level");
        String oddNum = request.getParameter("oddNum");
        String noun1 = request.getParameter("noun1");
        String bestFriend = request.getParameter("bestFriend");
        String football = request.getParameter("football");
        String adj1 = request.getParameter("adj1");
        String verbNoun = request.getParameter("verbNoun");
        String animal = request.getParameter("animal");
        String famous = request.getParameter("famous");
        String evenNum = request.getParameter("evenNum");
        String largeNum = request.getParameter("largeNum");
        String guarded = request.getParameter("guarded");
        String bodyPart2 = request.getParameter("bodyPart2");
        String adj2 = request.getParameter("adj2");
        String german = request.getParameter("german");
        String ingVerb2 = request.getParameter("ingVerb2");
        String nonprop1 = request.getParameter("nonprop1");
        String nonprop2 = request.getParameter("nonprop2");
        String noun2 = request.getParameter("noun2");
        String takeOver = request.getParameter("takeOver");
        String noun3 = request.getParameter("noun3");
        String verb2 = request.getParameter("verb2");
        String verb3 = request.getParameter("verb3");
        String smallNum = request.getParameter("smallNum");


        out.println("<h1><b>RESTRICTED TO PERSONNEL WITH SECURITY CLEARANCE " + level + " AND ABOVE" +
                " BY ORDER OF 05 COMMAND</b></h1>");
        out.println("<p><b>Item #:</b> SCP-" + oddNum + "-J</p>");
        out.println("<p><b>Object Class:</b> " + threat + "</p>");
        out.println("<p><b>Special Containment Procedures:</b> SCP-" + oddNum + "-J is to be kept in a " + foodType +
                "-lined containment chamber located in "+ guarded + ", where it is to be guarded at all " +
                "times by no less than " + smallNum + " " + profession + " armed with " + cheap + ".</p>");
        out.println("<p>In the event that SCP-" + oddNum + "-J ever begins " + ingVerb1 + " its " + bodyPart +
                ", Doctor " + bestFriend + " is to " + verb1 + " SCP-" + oddNum + "-J until it ceases its behavior. In" +
                " the event of a containment breach, Mobile Task Force " + greek + "-7 (Designation: \"" + tv + "\") is to be" +
                " dispatched to SCP-" + oddNum + "-J's last known location.</p><p>It is also of note that use of a " +
                adj2 + " " + noun3 + " can slightly aid in the pacification of SCP-" + oddNum + "-J for recontainment.</p>");
        out.println("<p><b>Description:</b> SCP-" + oddNum + "-J is a " + adj1 + " " + animal + ". Like most " +
                "members of its species, it is able to " + verbNoun + ", and regularly eats twice its own weight in " +
                foodType + " each day.</p><p>SCP-" + oddNum + "-J's unusual properties manifest whenever it comes in" +
                " contact with " + nonprop1 + ", which causes it to turn into a/an " + noun2 + ". Whenever this happens, " +
                "all " + nonprop2 + " within a " + evenNum + " kilometer radius will begin to " + verb2 +
                " uncontrollably, usually leading to civilian casualties.</p><p>In addition, many researchers feel " +
                "it has an uncanny resemblance to " + famous + ". Whether or not this is at all related to " +
                "SCP-" + oddNum + "-J's anomalous properties is unknown at this time.</p>" +
                "<p><b>RecoveryLog:</b> SCP-" + oddNum + "-J was first located in " + hometown + " where the "
                + football + " were using it in order to " + takeOver + ". Thankfully, Mobile Task Force " + greek + "" +
                "-7 (\"" + tv + "\") was able to recover the object with only " + largeNum + " civilian casualties.</p>" +
                "<p><b>Unconfirmed Anomalous Properties:</b></p>");
        try {
            out.println("<ul>");
            for (String list : tags) {
                out.println("<li>" + list + "</li>");
            }
            out.print("</ul>");
        } catch (NullPointerException e) {
            out.println("<li>None recorded.</li></ul>");
        }
                out.println("<p><b>Addendum:</b> Test Log " + oddNum + "-J-1</p>" +
                "<p><b>Dr. " + german + ":</b> Ello? Ello? Is zhis thing on? Ach, good. Zhis is Docktorr " + german +
                        ", and I am about to test SCP-" + oddNum + "-J's reaction to " + noun1 + ". Are " +
                        "you ready to proceed, Docktorr " + lastName + "?</p>" +
                "<p><b>Dr. " + lastName + ":</b> Yes sir, ready to begin test.</p>" +
                "<p><b>Dr. " + german + ":</b> Excellent! I am now introducing the " + noun1 + " to " + oddNum +
                        "-J... Hmm, zhe subject seems to have already figured out zhe test material.</p>" +
                "<p><b>Dr. " + lastName + ":</b> Making a note; 'subject shows high capacity for learning.'</p> "+
                "<p><b>Dr. " + german + ":</b> Now zhe subject is looking right at me, almost as if it... MEIN GOTT!" +
                        " MEIN " + bodyPart2 + "! IT'S GOT MEIN " + bodyPart2 + "! OH ZHE AGONY! ZHE " +
                        "AGONEEEEEEEEEY!</p><p>[DATA EXPUNGED]</p>" +
                "<p><b>END LOG</b></p>" +
                "<p><em>In light of incident " + oddNum + "-J-1 and its further " + ingVerb2 + " of </em>[DATA EXPUNGED]<em>" +
                        ", resulting in Dr. " + lastName + "'s immediate request for psychiatric consultation (and a request" +
                        " for Clorox brand bleach for use as \"eye bleach\",testing has been suspended indefinitely. -05-X</p>" +
                        "<p>Request for psychiatric consultation: Approved</p>" +
                        "<p>Request for Clorox brand bleach: Denied</em></p>");

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
            response.setContentType("text/html");
            String lastName = request.getParameter("lastName");

            if ((lastName == null) || (lastName.equals(""))) {
                throw new IllegalArgumentException("'LAST NAME' was left blank. Go back to the last page to fix.");
            }

        String sNum = request.getParameter("smallNum");
            try {
                int sNum2 = Integer.parseInt(sNum);
            } catch (NumberFormatException e) {
                System.out.println("Not a number");
            }

            if ((sNum == null) || (sNum.equals(""))) {
                throw new IllegalArgumentException("'Small Number was left blank. Go back and fix.");
            }
            int sNum3 = Integer.parseInt(sNum);
            if (sNum3 < 2) {
                throw new IllegalArgumentException("Small number must be 2 or greater. Go back and fix.");
            }

    }

}
